package com.designpaterns;

public interface FlyBehavior {
    public void fly();
}
