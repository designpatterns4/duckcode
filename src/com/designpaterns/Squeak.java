package com.designpaterns;

public class Squeak implements QuackBehavior{
    @Override
    public void quack() {
        System.out.println("I must be a rubber Duck, so SQUEEEAK!!");
    }
}
