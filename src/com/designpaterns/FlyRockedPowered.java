package com.designpaterns;

public class FlyRockedPowered implements FlyBehavior{
    @Override
    public void fly() {
        System.out.println("I'm flying with a Rocket!!");
    }
}
