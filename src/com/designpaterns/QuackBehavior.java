package com.designpaterns;

public interface QuackBehavior {
    public void quack();
}
