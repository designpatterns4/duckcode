package com.designpaterns;

public class Quack implements QuackBehavior {

    @Override
    public void quack() {
            System.out.println("I'm a real duck, so Quack quack quack!!");
    }
}
